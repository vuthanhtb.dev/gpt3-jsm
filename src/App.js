import React from 'react';
import {
  BrandComponent,
  CTAComponent,
  NavbarComponent
} from 'components';
import {
  BlogContainer,
  FeaturesContainer,
  FooterContainer,
  HeaderContainer,
  PossibilityContainer,
  WhatGPT3Container
} from 'containers';

const App = () => {
  return (
    <div className="app">
      <div className="gradient__bg">
        <NavbarComponent />
        <HeaderContainer />
      </div>
      <BrandComponent />
      <WhatGPT3Container />
      <FeaturesContainer />
      <PossibilityContainer />
      <CTAComponent />
      <BlogContainer />
      <FooterContainer />
    </div>
  );
};

export default App;
